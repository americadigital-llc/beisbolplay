<?php
/**
 * beisbolplay functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package beisbolplay
 */

if ( ! function_exists( 'beisbolplay_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function beisbolplay_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on beisbolplay, use a find and replace
		 * to change 'beisbolplay' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'beisbolplay', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu' => esc_html__( 'Menu', 'beisbolplay' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'beisbolplay_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'beisbolplay_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function beisbolplay_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'beisbolplay_content_width', 640 );
}
add_action( 'after_setup_theme', 'beisbolplay_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function beisbolplay_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'beisbolplay' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'beisbolplay' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'beisbolplay_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function beisbolplay_scripts() {
	wp_enqueue_style( 'beisbolplay-style', get_stylesheet_uri() );

	wp_enqueue_script( 'beisbolplay-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'beisbolplay-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'beisbolplay_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


//////////////////////////////////////////////////////////////////////////////////////////
// Que cada autor sólo pueda ver sus artículos
function query_set_only_author( $wp_query ) {
    global $current_user;
    if ( is_admin() && !current_user_can('manage_options') ) {
        $wp_query->set( 'author', $current_user->ID );
    }
}
add_action('pre_get_posts', 'query_set_only_author' );


if ( current_user_can('contributor') && !current_user_can('upload_files') ) {
    add_action('admin_init', 'allow_contributor_upload_files');
}
function allow_contributor_upload_files() {
    $contributor = get_role('contributor');
    $contributor->add_cap('upload_files');
}


function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


add_action( 'rest_api_init', 'bp_add_custom_rest_fields' );
function bp_add_custom_rest_fields() {
	//Video url custom field
    $bp_video_schema = array(
        'description'   => 'Platform video url',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'video_field',
        array(
            'get_callback'      => 'bp_get_video_field',
            'update_callback'   => null,
            'schema'            => $bp_video_schema
        )
    );
    //Featured images
    $bp_imgft_schema = array(
        'description'   => 'Next Post Featured Image',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'imgft_field',
        array(
            'get_callback'      => 'bp_get_imgft_field',
            'update_callback'   => null,
            'schema'            => $bp_imgft_schema
        )
    );
    //Custom date format
    $bp_datep_schema = array(
        'description'   => 'Custom date post',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
     
    // registering the bs_author_name field
    register_rest_field(
        'post',
        'datep_field',
        array(
            'get_callback'      => 'bp_get_datep',
            'update_callback'   => null,
            'schema'            => $bp_datep_schema
        )
    );
}
function bp_get_video_field( $object, $field_name, $request ) {
    return get_post_meta( $object['id'], $field_name, true );
}
function bp_get_imgft_field( $object, $field_name, $request ) {
    return get_the_post_thumbnail_url( $object['id'], 'full' );
}
function bp_get_datep( $object, $field_name, $request ) {
    return get_the_date( 'M d', $object['id'] );
}
add_image_size( 'medium', 300, 300, array( 'center', 'center' ) );

 

