/*global -$ */
'use strict';
const gulp = require('gulp')
const $ = require('gulp-load-plugins')()
const sourcemaps = require('gulp-sourcemaps');
const del = require('del')
const sass = require('gulp-sass');
const sassGlobing = require('node-sass-globbing');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

gulp.task('styles:materialize', function () {
  return gulp.src('./src/assets/css/libraries/materialize/sass/materialize.scss')
    .pipe(sass()
    .on('error', sass.logError))
    .pipe(gulp.dest('./demo/gridComponents/css/'))
    .pipe(reload({stream: true}));
});

gulp.task('styles:dev', ['styles:materialize'], function () {
  return gulp
    .src('./src/assets/css/main.scss')
    .pipe(concat('style.css'))
    .pipe(sass({
      outputStyle: 'uncompressed', // 'uncompressed',
      importer: sassGlobing
    })
    .on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(reload({stream: true}));
});

gulp.task('styles:dist', ['styles:materialize'],function () {
  return gulp
    .src('./src/assets/css/main.scss')
    .pipe(concat('style.css'))
    .pipe(sass({
      outputStyle: 'expanded', // 'uncompressed',
      importer: sassGlobing
    })
    .on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css/'));
    
});

gulp.task('clean', require('del').bind(null, ['dist']));

gulp.task('sync:site', () => {
  var serverConf = {
    server: {
      baseDir: "./public/assets",
      directory: true
    },
    open: true,
    injectChanges: true,
    serveStatic: ['.', './public/assets'],
    serveStaticOptions: {
        extensions: ['html'] // pretty urls
    }
  };
  browserSync.init(serverConf);
});

gulp.task('watch', function () {
  gulp.watch('./src/assets/css/**/*.scss', ['styles:dist']);
});

gulp.task('default', ['styles:dev','sync:site', 'watch']);

