import {
  INIT_POST,
  INIT_PAGE_ALL,
  INIT_CATEGORIES,
  INIT_REL,
  FILTER,
  ORDER,
  ADD_ITEM_MENU,
  ADD_PAGE,
  ADD_TAG,

} from '../actions'

const initialState = {
  post:[],
  comments: [],
  categories: [],
  postrel:[],
  menus:[],
  user:[],
  pages:[],
  tags:[],
}

function reducer(state =  initialState, action = {}){

  switch (action.type){
    case INIT_POST: {
      return {
        ...state,
        post: [...state.post, action.post]
      }
    }

    case INIT_PAGE_ALL: {
      return {
        ...state,
        page: action.page
      }
    }

    case INIT_CATEGORIES: {
      return {
        ...state,
        categories:  [...state.post, action.categories]
      }
    }

    case INIT_REL: {
      return {
        ...state,
        postrel:  [...state.postrel, action.postrel]
      }
    }

    case FILTER: {
      return {
        ...state,
        post: {...state.post,
          filter: action.filter
        }
      }
    }

    case ORDER: {
      console.log(action.order)
      switch (action.order){
        case 'lastvoted': {
          let arraynew = state.post.sort((a, b) => a.voteScore + b.voteScore)
          return {
            ...state,
            post: { arraynew }
          }
        }
        case 'morevoted': {
          let arraynew = state.post.sort((a, b) => a.voteScore - b.voteScore)
          return {
            ...state,
            post: { arraynew }
          }
        }
        case 'date': {
          let arraynew = state.post.sort((a, b) => a.timestamp + b.timestamp)
          return{
            ...state,
            post: { entities: arraynew }
          }
        }
        case 'all': {
          let arraynew = state.post.sort((a, b) => a.timestamp - b.timestamp)
          return{
            ...state,
            post: { entities: arraynew }
          }
        }
        default:{
          return state
        }
      }
    }
    
    case ADD_ITEM_MENU: {
      action.itemMenu.cat = action.slug
      let item = action.itemMenu
      return {
        ...state,
        menus:[...state.menus, item]
      }
    }

    case ADD_PAGE: {
      return {
        ...state,
        pages: action.PageObj
      }
    }

    case ADD_TAG: {
      return {
        ...state,
        tags: action.TagsObj
      }
    }

    default:{
      return state
    }
  }

}

export default reducer