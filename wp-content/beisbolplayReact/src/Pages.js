import React, { Component } from 'react';
import { Redirect } from 'react-router'
import renderHTML from 'react-render-html';
import RedSocial from './redsocial'
import Menu from './Menu'

import { connect } from 'react-redux'
import { initMenuAsync, PostCategoryAsync, PagebySlug } from './actions'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      pageObj:[],
      menu:[],
      redirect: false,
    }
  }

  componentDidMount(){
    if(this.props.match.params.pagePath !== '404'){
      this.props.getPage(this.props.match.params.pagePath)
      this.props.getMenuInit()
    }else{
      this.setState({
        redirect: true
      });
    }
  }

  componentWillReceiveProps(next_props) {
    if(next_props.pageObj !== this.state.pageObj){
      this.setState({
        pageObj: next_props.pageObj
      });
    }   
  }

  render() {
    if (this.state.redirect ){
      return <Redirect to='/error/404'/>;
    }
    return (
      <div className="detail category">
        <div className="section no-pad-bot" id="banner-up-tag">
          <div className="container">
            <div id="ad-slot"></div>
          </div>
        </div>
        <div className="cont-nav">
          <div className="lighten-1 hide-on-med-only">
            <div className="nav-wrapper container">
              <div className="row">
                <div className="col s6 offset-s6 right-align social-ico">
                  <RedSocial />
                </div>
              </div>
            </div>
          </div>
          <nav className="lighten-nav"  >
            <div className="nav-wrapper container">
              <Menu slug="menu"/>
            </div>
          </nav>
          <div className="cont-ban-detail">
          </div>
        </div>
        <div className="section cont-post">
          <div className="container">
            <div className="row">
              <div className="col s12 m12 l12">
              {
                this.state.pageObj.map((item, i) => {  
                  return (
                    <div className="item-post" key={i}>
                      <div className="cont-miga category-miga">
                        <p>{item.title.rendered}</p>
                      </div>
                      <div className="row">
                        { item.video_field.length > 0 &&
                          <div className="wrapper-player">
                            <div className="player">
                              <iframe src={item.video_field} title={item.title.rendered} frameborder="0"></iframe>
                            </div>
                          </div>
                        }
                        { item.featured_media > 0 &&
                          <div className="col s12 m12 l12">
                            <div className={(item.video_field !== '' && item.video_field !== undefined && item.video_field.length > 0) ? 'item-post-img-play' : 'item-post-img'}>
                              <a href={item.link}><img src={item.media.media_details.sizes.medium.source_url} alt=""/></a>
                            </div>
                          </div>
                        }
                        { item.featured_media > 0 &&
                          <div className="col s12 m12 l12">
                            <div className="item-post-cont">
                              <div>{renderHTML(item.content.rendered)}</div>
                            </div>
                          </div>
                        }
                        { item.featured_media === 0 &&
                          <div className="col s12 m12 l12">
                            <div className="item-post-cont">
                              <div>{renderHTML(item.content.rendered)}</div>
                            </div>
                          </div>
                        } 
                      </div>
                    </div>
                  )
                })
              }
              </div>
            </div>
          </div>
        </div>
        <footer className="page-footer">
          <div className="container">
            <div className="row">
              <div className="col s12 m3 l3 cont-logo">
                <a id="logo-container" href="/" className="brand-logo">
                  <img src="/wp-content/themes/beisbolplay/images/logo.png" alt=""/>
                </a>
              </div>
              <div className="col s12 m9 l9 right-align social-ico">
                <RedSocial />
              </div>
            </div>
            <div className="row">
              <div className="col s12 m4 l4 cont-logo-ter">
                <ul>
                  <li className="col s12 m7 l7"><a href="/terms-and-conditions/">TERMINOS DE USO</a></li>
                  <li className="col s12 m5 l5"><a href="mailto:contacto@beisbolplay.com">CONTACTO</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}


function mapStateToProps (reducer){
  return {
    menus: reducer.menus,
    pageObj: reducer.pages
  }
}

function mapDispathToProps (dispatch){
  return {
    getMenuInit: (data) => dispatch(initMenuAsync(data)),
    getCategory: (data) => dispatch(PostCategoryAsync(data)),
    getPage: (data) => dispatch(PagebySlug(data)),
  }
}

export default connect(mapStateToProps, mapDispathToProps)(App)
