import React, { Component } from 'react';
import renderHTML from 'react-render-html';

class Columns extends Component {

  constructor(props){
    super(props)
    this.state = {
      colObjs:[],
    }
  }

  componentDidMount(){
    this.setState({ 
      colObjs: this.props.colObj
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.colObj !== this.state.colObj){
      this.setState({ 
        colObjs: next_props.colObj
      })
    }
  }

  render() {
    return (
      <div className={ (this.state.colObjs.length <=0 )?'cont-lector hide': 'cont-lector'}>
        <div className="title-cont">
          <h3>Columnas</h3>
        </div>
        <div className="list-lector-cont">
          { 
            this.state.colObjs.map((item, i) => { 
              return (
                <div className="item-lector" key={i}>
                  <a href={item.link}>
                    { item.featured_media > 0 &&
                      <div className="item-img">
                        <img src={item.media.media_details.sizes.medium.source_url} alt=""/>
                      </div>
                    }
                    { item.featured_media > 0 &&
                      <div className="item-text">
                        <h4>{renderHTML(item.title.rendered)}</h4>
                      </div>
                    }
                    { item.featured_media === 0 &&
                      <div className="item-text">
                        <h4>{renderHTML(item.title.rendered)}</h4>
                      </div>
                    }
                  </a>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default Columns
