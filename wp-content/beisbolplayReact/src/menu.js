import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Navbar, NavItem} from 'react-materialize';

class Menu extends Component {
  constructor(props){
    super(props)
    this.state = {
      categoryObj:[],
      menu:[],
      options:{
        edege: 'right'
      }
    }
  }

  componentDidMount(){
    let menudata = this.props.menus.filter((itemdata) => {
      return itemdata.cat === this.props.slug
    })

    this.setState({
      menu: menudata
    });
  }

  componentWillReceiveProps(next_props) {
    if(next_props.menus !== this.state.menu){
      let menudata = this.props.menus.filter((itemdata) => {
        return itemdata.cat === this.props.slug
      })
      this.setState({
        menu: menudata
      });
    }
  }

  render() {
    return (
      <div>
        <Navbar right options={this.state.options}>
          <NavItem href='/'>Inicio</NavItem>
          {
            this.state.menu.map((item, i) => {  
              return (
                <NavItem key={i} href={item.url}>{item.title}</NavItem>
              )
            })
          }
        </Navbar>
      </div>
    );
  }
}


function mapStateToProps (reducer){
  return {
    menus: reducer.menus
  }
}

function mapDispathToProps (dispatch){
  return {
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Menu)
