import * as ReadableAPI from '../ReadableApi'

export const INIT_POST = 'INIT_POST'
export const INIT_PAGE_ALL = 'INIT_PAGE_ALL'

export const INIT_CATEGORIES = 'INIT_CATEGORIES'
export const INIT_REL = 'INIT_REL'
export const FILTER = 'FILTER'
export const ORDER = 'ORDER'
export const PAGE = 'PAGE'
export const ADD_ITEM_MENU = 'ADD_ITEM_MENU'
export const ADD_PAGE = 'ADD_PAGE'
export const ADD_TAG = 'ADD_TAG'


export function initPageAll (page) {
  return {
    type:'INIT_PAGE_ALL',
    page
  }
}

export function initPost (post) {
  return {
    type:'INIT_POST',
    post
  }
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

export function initPostAsync(exclude) {
  return dispatch => {
    ReadableAPI.getPostAll(exclude).then((postObj) => {
      dispatch(initPageAll(postObj['page']))
      let data = postObj['data']
      Object.keys(data).map(function (key) {
        data[key]['categoryObj']=[]
        data[key]['media']=[]
        if (data[key]['categories']){
          data[key]['categories'].map(function (idCat) {
            return ReadableAPI.getCategorybyid (idCat).then((mediaObj) => {
              if(mediaObj.name !== "Slider" && mediaObj.name !== "Columns"){
                data[key]['categoryObj']= data[key]['categoryObj'].concat(mediaObj)
              }
            })
          })
        }

        if (data[key]['featured_media'] !==  '0' && data[key]['featured_media'] !== 0 && data[key]['featured_media'] !== "0"){
          // ReadableAPI.getMediabyid(data[key]['featured_media']).then((mediaObj) => {
          //   if(Object.keys(mediaObj.media_details.sizes).length === 0 ){
          //       data[key]['featured_media'] = 0
          //     }
          //   data[key]['media']= mediaObj
          //   dispatch(initPost(data[key]))
          // })
          let mediaObj = data[key]["_embedded"]["wp:featuredmedia"]
          Object.keys(mediaObj).map(function (mey) {
            if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
              data[key]['featured_media'] = 0
            }
            data[key]['media']= mediaObj[mey]
            return true
          })
        }
        dispatch(initPost(data[key]))
        return true
      },{})
    })
  }
}

export function PostPageAsync(page, exclude) {
  return dispatch => {
    ReadableAPI.getPostPage(page, exclude)
      .then((postObj) => {
        Object.keys(postObj).map(function (key) {
          if (postObj[key]['categories']){
            ReadableAPI.getCategorybyid (postObj[key]['categories'][0]).then((mediaObj) => {
              postObj[key]['categoryObj']= mediaObj
            })
          }else{
            postObj[key]['categoryObj']=[]
          }
          postObj[key]['media']=[]
          if (postObj[key]['featured_media'] !==  '0' && postObj[key]['featured_media'] !== 0 && postObj[key]['featured_media'] !==  "0"){
            let mediaObj = postObj[key]["_embedded"]["wp:featuredmedia"]
            Object.keys(mediaObj).map(function (mey) {
              if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                postObj[key]['featured_media'] = 0
              }
              postObj[key]['media']= mediaObj[mey]
              return true
            })
          }
          dispatch(initPost(postObj[key]))
          return true
        },{})
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

export function CategoryPostPageAsync(page, idCat) {
  return dispatch => {
    ReadableAPI.getPostPageCategory(page, idCat)
      .then((postObj) => {
        Object.keys(postObj).map(function (key) {
          if (postObj[key]['categories']){
            ReadableAPI.getCategorybyid (postObj[key]['categories'][0]).then((mediaObj) => {
              postObj[key]['categoryObj']= mediaObj
            })
          }else{
            postObj[key]['categoryObj']=[]
          }
          postObj[key]['media']=[]
          if (postObj[key]['featured_media'] !==  '0' && postObj[key]['featured_media'] !== 0 && postObj[key]['featured_media'] !==  "0"){
            let mediaObj = postObj[key]["_embedded"]["wp:featuredmedia"]
            Object.keys(mediaObj).map(function (mey) {
              if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                postObj[key]['featured_media'] = 0
              }
              postObj[key]['media']= mediaObj[mey]
              return true
            })
          }
          dispatch(initPost(postObj[key]))
          return true
        },{})
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

export function PostSlugAsync(slug) {
  return dispatch => {
    ReadableAPI.getPostbySlug(slug)
      .then((postObj) => {
        Object.keys(postObj).map(function (key) {
          if(Object.size(postObj[key]['tags']) > 0){
            ReadableAPI.getPostbyTagid(postObj[key]['tags'], postObj[key]['id']).then((tagObj) => {
              postObj[key]['related']= tagObj
            })
          }else{
            postObj[key]['related']=[]
          }          if (postObj[key]['categories']){
            ReadableAPI.getCategorybyid (postObj[key]['categories'][0]).then((mediaObj) => {
              postObj[key]['categoryObj']= mediaObj
            })
          }else{
            postObj[key]['categoryObj']=[]
          }
          postObj[key]['media']=[]
          if (postObj[key]['featured_media'] !==  '0' && postObj[key]['featured_media'] !== 0 && postObj[key]['featured_media'] !==  "0"){
            let mediaObj = postObj[key]["_embedded"]["wp:featuredmedia"]
            Object.keys(mediaObj).map(function (mey) {
              if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                postObj[key]['featured_media'] = 0
              }
              postObj[key]['media']= mediaObj[mey]
              return true
            })
          }
          dispatch(initPost(postObj[key]))
          return true
        },{})
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

export function array_rand(array){
     return array[~~(Math.random()*(array.length))];
}

export function PostnotSlugAsync(slug) {
  return dispatch => {
    ReadableAPI.getPostbySlug(slug)
      .then((postObj) => {
        Object.keys(postObj).map(function (key) {
          postObj[key]['porsrel'] = []
          postObj[key]['media']=[]
          ReadableAPI.getCategorybyid(postObj[key]['categories'][0])
            .then((CatObj) =>{
              dispatch(initCategories(CatObj))
            })
          ReadableAPI.getPostbyCategory(postObj[key]['categories'][0])
            .then((postObj) => {
              dispatch(initPageAll(postObj['page']))
              let data = postObj['data']
              data[key]['porsrel'] = []
              Object.keys(data).map(function (key) {
                if(Object.size(data[key]['tags']) > 0){
                  ReadableAPI.getPostbyTagid(data[key]['tags'], data[key]['id'])
                    .then((tagsObj) => {
                      let elemone = array_rand(tagsObj)
                      let elemtwo = array_rand(tagsObj)
                      if (elemone !== elemtwo){
                        let itemsrel = []
                        itemsrel = itemsrel.concat(elemone)
                        itemsrel = itemsrel.concat(elemtwo)
                        Object.keys(itemsrel).map(function (key) {
                          itemsrel[key]['media'] = []
                          if (itemsrel[key]['featured_media'] !==  '0' && itemsrel[key]['featured_media'] !== 0 && itemsrel[key]['featured_media'] !==  "0"){
                            let mediaObj = itemsrel[key]["_embedded"]["wp:featuredmedia"]
                            Object.keys(mediaObj).map(function (mey) {
                              if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                                itemsrel[key]['featured_media'] = 0
                              }
                              itemsrel[key]['media']= mediaObj[mey]
                              return true
                            })

                          }
                          return true
                        },{})
                        data[key]['porsrel'] = itemsrel
                        dispatch(initRel(data[key]))
                      }
                    })
                    .catch(()=>{
                      console.log('error')
                    })
                }
                return true;
              },{})
            })
            .catch(()=>{
              console.log('error')
            })
          if(Object.size(postObj[key]['tags']) > 0){
            ReadableAPI.getPostbyTagid(postObj[key]['tags'], postObj[key]['id'])
              .then((tagsObj) => {
                let elemone = array_rand(tagsObj)
                let elemtwo = array_rand(tagsObj)
                if (elemone !== elemtwo){
                  let itemsrel = []
                  itemsrel = itemsrel.concat(elemone)
                  itemsrel = itemsrel.concat(elemtwo)
                  
                  Object.keys(itemsrel).map(function (key) {
                    itemsrel[key]['media']=[]
                    if (itemsrel[key]['featured_media'] !==  '0' && itemsrel[key]['featured_media'] !== 0 && itemsrel[key]['featured_media'] !==  "0"){
                      let mediaObj = itemsrel[key]["_embedded"]["wp:featuredmedia"]
                      Object.keys(mediaObj).map(function (mey) {
                        if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                          itemsrel[key]['featured_media'] = 0
                        }
                        itemsrel[key]['media']= mediaObj[mey]
                        return true
                      })
                    }
                    return true
                  },{})
                  postObj[key]['porsrel'] = itemsrel
                }
              })
              .catch(()=>{
                console.log('error')
              })
          }
          Object.keys(postObj).map(function (key) {
            if (postObj[key]['featured_media'] !==  '0' && postObj[key]['featured_media'] !== 0 && postObj[key]['featured_media'] !==  "0"){
              let mediaObj = postObj[key]["_embedded"]["wp:featuredmedia"]
              Object.keys(mediaObj).map(function (mey) {
                if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                  postObj[key]['featured_media'] = 0
                }
                postObj[key]['media']= mediaObj[mey]
                return true
              })
            }
            dispatch(initPost(postObj[key]))
            return true
          },{})
          return true
        },{})
      })
      .catch((e)=>{
        console.log('error post' + e)
      })

  }
}


export function PostbyCategorySlug(slug) {
  return dispatch => {
    ReadableAPI.getCategorybySlug(slug)
      .then((CatObj) => {
        if(Object.size(CatObj) === 0){
          let cat = {}
          cat.name = "Resultados de Categoria"
          cat.slug = "search"
          cat.id = 9999999
          cat.description = ""
          cat.meta = []
          cat.parent = 0
          cat.taxonomy = "category"
          cat.count = 0
          dispatch(initCategories(cat))

        }else{
          Object.keys(CatObj).map(function (key) {
            dispatch(initCategories(CatObj[key]))
            ReadableAPI.getPostbyCategory(CatObj[key]['id'])
              .then((postObj) => {
                dispatch(initPageAll(postObj['page']))
                let data = postObj['data']
                Object.keys(data).map(function (key) {
                  if (data[key]['categories']){
                    ReadableAPI.getCategorybyid (data[key]['categories'][0]).then((mediaObj) => {
                      data[key]['categoryObj']= mediaObj
                    })
                  }else{
                    data[key]['categoryObj']=[]
                  }
                  data[key]['media']=[]
                  if (data[key]['featured_media'] !==  '0' && data[key]['featured_media'] !== 0 && data[key]['featured_media'] !==  "0"){
                    let mediaObj = data[key]["_embedded"]["wp:featuredmedia"]
                    Object.keys(mediaObj).map(function (mey) {
                      if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                        data[key]['featured_media'] = 0
                      }
                      data[key]['media']= mediaObj[mey]
                      return true
                    })
                  }
                  dispatch(initPost(data[key]))
                  return true
                },{})
              })
              .catch(()=>{
                console.log('error')
              })
            return true
          },{})
        }
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

export function PostCategoryAsync(idMedia) {
  return dispatch => {
    ReadableAPI.getPostbyCategorybyid(idMedia)
      .then((postObj) => {
        Object.keys(postObj).map(function (key) {
          postObj[key]['media']=[]
          if (postObj[key]['categories']){
            ReadableAPI.getCategorybyid (postObj[key]['categories'][0]).then((mediaObj) => {
              postObj[key]['categoryObj']= mediaObj
            })
          }else{
            postObj[key]['categoryObj']=[]
          }
          if (postObj[key]['featured_media'] !==  '0' && postObj[key]['featured_media'] !== 0 && postObj[key]['featured_media'] !==  "0"){
            // ReadableAPI.getMediabyid(postObj[key]['featured_media']).then((mediaObj) => {
            //   if(Object.keys(mediaObj.media_details.sizes).length === 0 ){
            //     postObj[key]['featured_media'] = 0
            //   }
            //   postObj[key]['media']= mediaObj
            //   dispatch(initPost(postObj[key]))
            // })
            let mediaObj = postObj[key]["_embedded"]["wp:featuredmedia"]
            Object.keys(mediaObj).map(function (mey) {
              if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                postObj[key]['featured_media'] = 0
              }
              postObj[key]['media']= mediaObj[mey]
              return true
            })
          }

          dispatch(initPost(postObj[key]))
          return true
        },{})
      })
      .catch((e)=>{
        console.log('error' + e)
      })

  }
}

export function initCategories (categories) {
  return{
    type:'INIT_CATEGORIES',
    categories
  }
}

export function initRel (postrel) {
  return{
    type:'INIT_REL',
    postrel
  }
}

export function getInitListCategories() {
  let arraycat = []
  return dispatch => {
    ReadableAPI.getCategoriesAll().then((categoriesObj) => {
      Object.keys(categoriesObj.categories).map(function (key) {
        return arraycat[key] = categoriesObj.categories[key]
      },{})
      dispatch(initCategories(arraycat))
      return arraycat
    })
  };
}

export function filter (filter) {
  return{
    type:'FILTER',
    filter
  }
}

export function order (order) {
  return{
    type:'ORDER',
    order
  }
}


export function initMenuAsync() {
  return dispatch => {
    ReadableAPI.getMenuAll().then((menuObj) => {
      Object.keys(menuObj).map(function(key){
        return dispatch(addMenuAsync(menuObj[key].slug))
      })
    })
  }
}

export function addItemMenu (itemMenu, slug) {
  return{
    type:'ADD_ITEM_MENU',
    slug,
    itemMenu
  }
}

export function addMenuAsync(slug) {
  return dispatch => {
    ReadableAPI.getMenubySlug(slug).then((menuObj) => {
      menuObj.items.forEach((element) => {
        dispatch(addItemMenu(element, slug))
      })
    })
  }
}

export function PageAdd (PageObj) {
  return{
    type:'ADD_PAGE',
    PageObj
  }
}

export function PagebySlug(slug) {
  return dispatch => {
    ReadableAPI.getPagesbySlug(slug)
      .then((PageObj) => {
        Object.keys(PageObj).map(function (key) {
          PageObj[key]['media']=[]
          if (!PageObj[key]['video_field']){
            PageObj[key]['video_field'] = []
          }
          if (PageObj[key]['featured_media'] !==  '0' && PageObj[key]['featured_media'] !== 0 && PageObj[key]['featured_media'] !==  "0"){
            let mediaObj = PageObj[key]["_embedded"]["wp:featuredmedia"]
            Object.keys(mediaObj).map(function (key) {
              if(Object.size(mediaObj[key]['media_details']['sizes']) === 0 ){
                PageObj[key]['featured_media'] = 0
              }
              PageObj[key]['media']= mediaObj[key]
              return true
            })
          }else{
            PageObj[key]['media']=[]
          }
          return true
        },{})
        dispatch(PageAdd(PageObj))
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

export function TagAdd (TagsObj) {
  return{
    type:'ADD_TAG',
    TagsObj
  }
}

export function PostbyTagSlug(slug) {
  return dispatch => {
    ReadableAPI.getTagsbySlug(slug)
      .then((TagObj) => {
        dispatch(TagAdd(TagObj))
        Object.keys(TagObj).map(function(key) {
          ReadableAPI.getPostbyTagid(TagObj[key]['id'])
          .then((PostObj) => {
            Object.keys(PostObj).map(function (key) {
              PostObj[key]['media']=[]
              if (!PostObj[key]['video_field']){
                PostObj[key]['video_field'] = []
              }
              if (PostObj[key]['featured_media'] !==  '0' && PostObj[key]['featured_media'] !== 0 && PostObj[key]['featured_media'] !==  "0"){
                let mediaObj = PostObj[key]["_embedded"]["wp:featuredmedia"]
                Object.keys(mediaObj).map(function (mey) {
                  if(Object.size(mediaObj[mey]['media_details']['sizes']) === 0 ){
                    PostObj[key]['featured_media'] = 0
                  }
                  PostObj[key]['media']= mediaObj[mey]
                  return true
                })
              }else{
                PostObj[key]['media']=[]
              }
              dispatch(initPost(PostObj[key]))
              return true
            })
          })
          .catch(()=>{
            console.log('error')
          })
          return true
        })
        
      })
      .catch(()=>{
        console.log('error')
      })

  }
}

