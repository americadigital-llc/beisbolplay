import React, { Component } from 'react';

class Itempost extends Component {
  constructor(props){
    super(props)
    this.state = {
      title:'',
      img:'',
      utl:''
    }
    this.shareFacebook =  this.shareFacebook.bind(this)
    this.shareTwitter =  this.shareTwitter.bind(this)
    this.shareGoogle =  this.shareGoogle.bind(this)
  }

  componentDidMount(){
    this.setState({ 
      title: this.props.textShared,
      img: this.props.imgShared,
      url: this.props.urlShared,
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.itemPost !== this.state.postObj){
      this.setState({ 
        title: next_props.textShared,
        img: next_props.imgShared,
        url: next_props.urlShared,
      })
    }
  }

  shareFacebook (){
    let url = `http://www.facebook.com/sharer.php?u=${this.state.url}&amp;t=${this.state.title }`
    window.open(url, 'facebookShare', 'width=626,height=436')
  }

  shareTwitter   (){
    let url = `http://twitter.com/share?text=${this.state.title }-&amp;url=${this.state.url}`
    window.open(url, 'twitterShare', 'width=626,height=436'); return false;
  }

  shareGoogle   (){
    let url = `https://plus.google.com/share?url=${this.state.url}`
    window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'); return false;
  }

  render() {
    return (
      <div>
        <a className="facebook"  onClick={this.shareFacebook} title="Share on Facebook" >
          <span className="icon-facebook"></span>
        </a>
        <a className="twitter" onClick={this.shareTwitter} title="Share on Twitter">
          <span className="icon-twitter"></span>
        </a>
        <a className="google" onClick={this.shareGoogle} title="Share on Google">
          <span className="icon-google-plus"></span>
        </a>
        <a className="whatzap hide-on-med-and-up" href={`whatsapp://send?text=${this.state.title } ${this.state.url}`}>
          <span className="icon-whatsapp"></span>
        </a>
        <a className="email" href="mailto:contacto@beisbolplay.com">
          <span className="icon-mail"></span>
        </a>
      </div>
    );
  }
}

export default Itempost
