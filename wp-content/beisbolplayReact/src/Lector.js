import React, { Component } from 'react';
import renderHTML from 'react-render-html';

class Columns extends Component {

  constructor(props){
    super(props)
    this.state = {
      artObj:[],
    }
  }

  componentDidMount(){
    this.setState({ 
      artObj: this.props.lector
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.lector !== this.state.artObj){
      this.setState({ 
        artObj: next_props.lector
      })
    }
  }

  render() {
    return (
      <div className={ (this.state.artObj.length <=0 )?'columns hide ': 'columns'}>
        <div className="header-col">
          <h3>del lector</h3>
        </div>
        <div className="list-col">
          { 
            this.state.artObj.map((item, i) => { 
              return (
                <div className="item-post-col" key={i}>
                  <div className="row">
                    { item.featured_media > 0 &&
                      <div className="col s4 m4 l4">
                        <div className="item-post-img">
                          <a href={item.link}><img src={item.media.media_details.sizes.thumbnail.source_url} alt=""/></a>
                        </div>
                      </div>
                      
                    }
                    { item.featured_media > 0 &&
                      <div className="col s8 m8 l8">
                        <div className="item-post-title">
                          <h3>{renderHTML(item.title.rendered)}</h3>
                        </div>
                        <div className="btn-more">
                          <a href={item.link}>Ver más</a>
                        </div>
                      </div>
                    }
                    { item.featured_media === 0 &&
                      <div className="col s8 m12 l12">
                        <div className="item-post-title">
                          <h3>{renderHTML(item.title.rendered)}</h3>
                        </div>
                        <div className="btn-more">
                          <a href={item.link}>Ver más</a>
                        </div>
                      </div>
                    }
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default Columns
