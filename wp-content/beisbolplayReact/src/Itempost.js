import React, { Component } from 'react';
import renderHTML from 'react-render-html'; 
import PostRelContent from './Postrel'
import SharepostContent from './Sharepost'
import MetaTags from 'react-meta-tags';
import VideoPlayer from './VideoPlayer';


class Itempost extends Component {
  constructor(props){
    super(props)
    this.state = {
      postObj:[],
      url:'',
    }
  }

  componentDidMount(){
    this.setState({ 
      postObj: this.props.itemPost,
      url: this.props.urlact
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.itemPost !== this.state.postObj){
      this.setState({ 
        postObj: next_props.itemPost,
        url: this.props.urlact
      })
    }
  }

  render() {
    const data = this.state.postObj

    return (
      <div>
      {
        data.map((item, i) => {
          let videoJsOptions = {}
          if(item.video_field.indexOf('youtu.be') !== -1 || item.video_field.indexOf('youtube.com') !== -1){
            videoJsOptions = {
              autoplay: false,
              controls: true,
              aspectRatio: '16:9',
              sources: [{
                src: item.video_field,
                type: "video/youtube"
              }]
            }
          }

          if(item.video_field.indexOf('vimeo.com') !== -1){
            videoJsOptions = {
              autoplay: false,
              controls: true,
              aspectRatio: '16:9',
              sources: [{
                src: item.video_field,
                type: "video/vimeo"
              }]
            }
          }

          if(item.video_field.indexOf('.mp4') !== -1){
            videoJsOptions = {
              autoplay: false,
              controls: true,
              aspectRatio: '16:9',
              sources: [{
                src: item.video_field,
                type: "video/mp4"
              }]
            }
          }
          return (
            <div className="item-post-content" key={i}>
              <MetaTags>
                <title>{renderHTML(item.title.rendered)}</title>
                <meta name="description" content={item.excerpt.rendered} />
                <meta property="og:url" content={item.link} />
                <meta property="og:title" content={renderHTML(item.title.rendered)} />
                { item.media > 0 &&
                  <meta property="og:image" content={item.media.media_details.sizes.full.source_url} />
                }
                { item.video_field.length > 0 &&
                  <meta property="og:video" content={item.video_field} />
                }
                <meta property="og:type" content="article" />
              </MetaTags>
              <div className="title">{renderHTML(item.title.rendered)}</div>
              <div className="date-cont">
                <div className="date"><p>{item.datep_field}</p></div>
                <div className="social">
                  <ul>
                    <SharepostContent textShared={item.title.rendered} imgShared="" urlShared={item.link}/>
                  </ul>
                </div>
              </div>
              <div>
              { item.video_field.length > 0 && item.imgft_field.length > 0 &&
                <div className="wrapper-player">
                  <div className="player-new">
                    <VideoPlayer { ...videoJsOptions } />
                  </div>
                </div>
              }

              { item.video_field.length > 0 && item.imgft_field.length === 0 &&
                <div className="wrapper-player">
                  <div className="player">
                    <iframe src={item.video_field} title={item.title.rendered} frameborder="0"></iframe>
                  </div>
                </div>
              }
              { item.video_field.length === 0 &&
                <div>
                  { item.imgft_field.length > 0 &&
                    <img src={item.imgft_field} alt=""/>
                  }
                </div>
              }
              </div>
              <div>{renderHTML(item.content.rendered)}</div>
              <div className="row">
                <PostRelContent post={(item.porsrel)?item.porsrel :[] } />
              </div>
            </div>
          )
        })
      }
      </div>
    );
  }
}

export default Itempost
