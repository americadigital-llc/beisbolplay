import createSymlink from 'create-symlink';
import {realpathSync} from 'fs';
 
createSymlink('../../maqueta/public/assets/css/style.css', 'style.css').then(() => {
  realpathSync('../../maqueta/public/assets/css/style.css'); //=> '/where/file/exists' 
});