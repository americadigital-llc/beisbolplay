import React, { Component } from 'react';
import renderHTML from 'react-render-html';

class PostRel extends Component {
  constructor(props){
    super(props)
    this.state = {
      postObj:[],
    }
  }

  componentDidMount(){
    this.setState({ 
      postObj: this.props.post
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.post !== this.state.postObj && next_props.post !== 'undefined' && next_props.post !== undefined){
      this.setState({ 
        postObj: next_props.post
      })
    }
  }

  render() { 
    return (
      <div className={ (this.state.postObj.length <=0 )?'rel-data hide': 'rel-data'}>
        <div className="title-rel">
          <h3>Recomendados</h3>
        </div>
        {
          this.state.postObj.map((item, i) => { 
            return (
              <div className="col s12 m6 l6" key={i}>
                <div className="cont-item">
                  { item.featured_media > 0 &&
                    <a href={item.link}>
                      <div className="item">
                        <div className={(item.video_field !== '') ? 'img-item-play' : 'img-item'}>
                        </div>
                        <div className="text-item">
                          <h3>{renderHTML(item.title.rendered)}</h3>
                        </div>
                      </div>
                    </a>
                  }
                  { item.featured_media === 0 &&
                    <a href={item.link}>
                      <div className="item">
                        <div className="text-item">
                          <h3>{renderHTML(item.title.rendered)}</h3>
                        </div>
                      </div>
                    </a>
                  }
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}


export default PostRel
