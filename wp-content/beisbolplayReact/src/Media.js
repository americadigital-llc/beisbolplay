import React, { Component } from 'react';
import renderHTML from 'react-render-html';

class Media extends Component {
  constructor(props){
    super(props)
    this.state = {
      postObj:[],
    }
  }

  componentDidMount(){
    this.setState({ 
      postObj: this.props.objmedia
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.objmedia !== this.state.mediaObj){
      this.setState({ 
        postObj: next_props.objmedia
      })
    }
  }

  render() { 
    const item = this.state.postObj
    return (
      <div className="row cont-items-media">
        <div className="col s12 m6 l6">
          <div className="cont-item">
            { item.length > 0 &&
              <a href={item[0].link}>
                <div className="item-play">
                  <div className={(item[0].video_field !== '') ? 'img-item-play' : 'img-item'}>
                    <img src={item[0].media.media_details.sizes.full.source_url} alt=""/>
                  </div>
                  <div className="text-item">
                    <h3>{renderHTML(item[0].title.rendered)}</h3>
                    <div className="btn-more">
                      <p>Ver más</p>
                    </div>
                  </div>
                </div>
              </a>
            }
          </div>
        </div>
        <div className="col s12 m6 l6">
          <div className="cont-item-img">
              { item.length > 2 &&
                <a href={item[1].link}>
                  <div className="item">
                    <div className={(item[1].video_field !== '') ? 'img-item-play' : 'img-item'}>
                      <img src={item[1].media.media_details.sizes.full.source_url} alt=""/>
                    </div>
                    <div className="text-item">
                      <h3>{renderHTML(item[1].title.rendered)}</h3>
                      <div className="btn-more">
                        <p>Ver más</p>
                      </div>
                    </div>
                  </div>
                </a>
              }
              { item.length > 3 &&
                <a href={item[2].link}>
                  <div className="item">
                    <div className={(item[2].video_field !== '') ? 'img-item-play' : 'img-item'}>
                      <img src={item[2].media.media_details.sizes.full.source_url} alt=""/>
                    </div>
                    <div className="text-item">
                      <h3>{renderHTML(item[2].title.rendered)}</h3>
                      <div className="btn-more">
                        <p>Ver más</p>
                      </div>
                    </div>
                  </div>
                </a>
              }
          </div>
        </div> 
        
      </div>
    );
  }
}


export default Media
