import React, { Component } from 'react';
import renderHTML from 'react-render-html';

class SliderContent extends Component {

  constructor(props){
    super(props)
    this.state = {
      slider: [],
      activeIndex: 1,
      left: 0
    }
    this.prevSlide = this.prevSlide.bind(this)
    this.nextSlide = this.nextSlide.bind(this)
    this.clickIndicator = this.clickIndicator.bind(this)
  }

  componentDidMount(){
    this.setState({ 
      slider: this.props.slider
    })
  }

  componentWillReceiveProps(next_props) {
    if(next_props.slider !== this.state.slider){
      this.setState({ 
        slider: next_props.slider
      })
    }
  }

  prevSlide() {
    this.setState({
      activeIndex: this.state.activeIndex - 1,
      left: this.state.left + 400 // this.props.sliderWidth not working for some reason
    })
    if (this.state.activeIndex === 1) {
      this.setState({
        activeIndex: this.state.activeIndex + this.state.slider.length - 1,
        left: this.state.left - this.props.sliderWidth * (this.state.slider.length - 1)
      })
    }
  }

  nextSlide() {
    this.setState({
      activeIndex: this.state.activeIndex + 1,
      left: this.state.left - this.props.sliderWidth
    })
    if (this.state.activeIndex === this.state.slider.length) {
      this.setState({
        activeIndex: this.state.activeIndex - this.state.slider.length + 1,
        left: 0
      })
    }
  }

  clickIndicator(e) {
    this.setState({
      activeIndex: parseInt(e.target.textContent, 10),
      left: this.props.sliderWidth - parseInt(e.target.textContent, 10) * this.props.sliderWidth
    })
  }


  render() {
    var style = {
      left: this.state.left,
      width: this.props.sliderWidth,
      height: this.props.sliderHeight
    };
    return (
      <div>
        <div  className="slider-wrapper">
          <div className="timer"></div>
          <ul className="slider">
            {
              this.state.slider.map(function(item,index) {
                return (
                  <li key={index} style={style} className={index+1 === this.state.activeIndex ? 'slider-item' : 'hide'}>
                    <a href={item.link}>
                      
                      <div className="cont-img">
                        <img src={item.media.media_details.sizes.full.source_url} alt=""/>
                      </div>
                      <div className="container flow-text">
                        <div className="row">
                          <div className="text-cont col s12 offset-m6 m6 offset-l6 l6">
                            <h2>{renderHTML(item.title.rendered)}</h2>
                            <div className="text-cont-ext">{renderHTML(item.excerpt.rendered)}</div>
                          </div>
                        </div>
                      </div>
                    </a>
                  </li>
                )
              },this)
            }
          </ul>
        </div>
        <div className="buttons-wrapper">
          <button className="prev-button" onClick={this.prevSlide}>
            <span className="icon-circle-left"></span>
          </button>
          <button className="next-button" onClick={this.nextSlide}>
            <span className="icon-circle-right"></span>
          </button>
        </div>
        <div className="indicators-wrapper">
          <ul className="indicators">
           {
            this.state.slider.map(function(item,index) {
              return (
              <li key={index} className={index+1 === this.state.activeIndex ? 'active-indicator' : ''}onClick={this.clickIndicator}>{index+1}</li>
              )
            },this)
          }
          </ul>
        </div>
      </div>
    )
  }
}

export default SliderContent
