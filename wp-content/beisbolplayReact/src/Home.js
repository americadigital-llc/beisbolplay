import React, { Component } from 'react';
import renderHTML from 'react-render-html';
import RedSocial from './redsocial'
import Menu from './Menu'
import Media from './Media'
import SliderContent from './Slider'
import ColumsContent from './Columns'
import LectorContent from './Lector'
import MetaTags from 'react-meta-tags';
//import logo from './logo.svg';

import { connect } from 'react-redux'
import { initCategories, initPostAsync, initMenuAsync, order, filter, PostPageAsync, PostCategoryAsync } from './actions'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      postObj:[],
      menu:[],
      article:[],
      loading: true,
      page:1,
      slideObj:[],
      MediaObj:[],
      columnObj:[],
      exludeCat:[5,12,9],
      scrolled:0,
      domain: window.location.hostname,
      logo: `${window.location.hostname}/wp-content/themes/beisbolplay/images/logo.png`
    }
    this.handlescroll = this.handlescroll.bind(this)
    this.sidebarscroll = this.sidebarscroll.bind(this)
  }

  componentDidMount(){
    this.props.getAllPostInit(this.state.exludeCat)
    this.props.getMenuInit()
    this.props.getCategory(5)
    this.props.getCategory(9)
    this.props.getCategory(12)

    window.addEventListener('scroll', this.handlescroll)
    window.addEventListener('scroll', this.sidebarscroll)
    this.setState({ 
      loading: false,
      page: this.state.page + 1
    })

    if(this.props.pageTotal <= this.state.page){
      this.setState({ 
        loading: false,
        page: this.state.page + 1
      })
    }else{
      this.setState({ 
        loading: false,
      })
    }
  }

  componentWillReceiveProps(next_props) {
    if(next_props.post !== this.state.postObj){

      let menudata = next_props.post.filter((itemdata) => {
        for (var i = itemdata.categories.length - 1; i >= 0; i--) {
          return itemdata.categories[i] === 12
        }
        return true;
      })
      this.setState({
        article: menudata
      });

      //console.log(next_props)

      let post = next_props.post.filter((itemdata) => {
        //console.log(itemdata.id)
        return itemdata.categories[0] !== 9 && itemdata.categories[0] !== 12
      })

      this.setState({
        postObj: post
      });

      let slidepost = next_props.post.filter((itemdata) => {
        for (var i = itemdata.categories.length - 1; i >= 0; i--) {
          return itemdata.categories[i] === 8
        }
        return true;
      })

      this.setState({
        slideObj: slidepost
      });

      let mediaPost = next_props.post.filter((itemMedia) => {
        for (var i = itemMedia.categories.length - 1; i >= 0; i--) {
          return itemMedia.categories[i] === 5
        }
        return true;
      })

      this.setState({
        MediaObj: mediaPost
      });

      let columnPost = next_props.post.filter((itemColumn) => {
        for (var i = itemColumn.categories.length - 1; i >= 0; i--) {
          return itemColumn.categories[i] === 9
        }
        return true;
      })

      let dat1=this.state.columnObj.length
      let dat2=columnPost.length

      if(dat1 < dat2){
        this.setState({
          columnObj: columnPost
        });
      }
    }   
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handlescroll)
    window.removeEventListener('scroll', this.sidebarscroll)
  }


  handlescroll(event){
    if(this.state.loading) return null

    const scrolled = window.scrollY
    const viewportHeight = window.innerHeight
    const fullHeight = document.body.clientHeight

    //console.log(!(scrolled + viewportHeight + 300 >= fullHeight))

    if (!(scrolled + viewportHeight + 300 >= fullHeight)){
      return null
    }

    if(this.state.page <= this.props.pageTotal){
      this.setState({ loading: true}, async () => {
        try{
          this.props.getPagePost(this.state.page, this.state.exludeCat) 
          this.setState({ 
            loading: false,
            page: this.state.page + 1
          })
        }
        catch (error){
          this.setState({ loading: false })
        }
      })
    }
  }

  sidebarscroll(event){
    if(this.state.loading) return null

    const scrolled = window.scrollY
    const viewportHeight = window.innerHeight
    const fullHeight = document.body.clientHeight
    const fullWidth = document.body.clientWidth
    const sidebarHeight = document.body.getElementsByClassName('sidebar__inner')[0].clientHeight
    const footerHeight = document.body.getElementsByClassName('page-footer')[0].clientHeight
    const sidebarPost = document.body.getElementsByClassName('sidebar')[0].offsetTop
    const totalPost = sidebarPost + sidebarHeight
    const viewHeigth = scrolled + viewportHeight
    const viewWindow = (fullHeight - footerHeight) - 50

    if(fullWidth > 430){

      if (!(scrolled + viewportHeight >= totalPost)){
        return null
      }

      if(viewHeigth >= totalPost && viewHeigth < viewWindow){
        let topNumber = viewHeigth - totalPost;
        document.body.getElementsByClassName('sidebar__inner')[0].style.top = `${topNumber}px`
      }
    }
  }

  render() { 
    return (
      <div>
        <MetaTags>
          <title>BeisbolPlay</title>
          <meta name="description" content="Web Site BeisbolPlay " />
          <meta property="og:url" content={this.state.domain} />
          <meta property="og:title" content="Beisbolplay" />
          <meta property="og:image" content={this.state.logo} />
          <meta property="og:type" content="Website" />
        </MetaTags>
        <div className="section no-pad-bot" id="banner-up-tag">
          <div className="container">
            <div id="ad-slot"></div>
          </div>
        </div>
        <div className="cont-nav">
          <div className="lighten-1 hide-on-med-only">
            <div className="nav-wrapper container">
              <div className="row">
                <div className="col s6 offset-s6 right-align social-ico">
                  <RedSocial />
                </div>
              </div>
            </div>
          </div>
          <nav className="lighten-nav"  >
            <div className="nav-wrapper container">
              <Menu slug="menu"/>
            </div>
          </nav>
          <div className="cont-ban">
            <SliderContent  slider={this.state.slideObj}/>
          </div>
        </div>
        <div className="section cont-media">
          <div className="container">
            <div className="row">
              <div className="section no-pad-bot" id="banner-up-tag">
                <div className="container">
                  <div id="ad-slot-1"></div>
                </div>
              </div>
            </div>
            <Media objmedia={this.state.MediaObj} />
          </div>
        </div>
        <div className="section cont-post">
          <div className="container main-content">
            <div className="row">
              <div className="col s12 m7 l8">
              {
                this.state.postObj.map((item, i) => {  
                  return (
                    <div className="item-post" key={i}>
                      <div className="row">
                      { item.featured_media > 0 &&
                        <div className="col s12 m4 l4">
                          <div className={(item.video_field !== '') ? 'item-post-img-play' : 'item-post-img'}>
                            <a href={item.link}><img src={item.media.media_details.sizes.medium.source_url} alt=""/></a>
                          </div>
                        </div>
                      }
                      { item.featured_media > 0 &&
                        <div className="col s12 m8 l8">
                          <div className="item-post-date">
                            <p>{item.datep_field}</p>
                          </div>
                          <div className="item-post-title">
                            <a href={item.link}><h3>{renderHTML(item.title.rendered)}</h3></a>
                          </div>
                          <div className="item-post-cont">
                            <div>{renderHTML(item.excerpt.rendered)}</div>
                          </div>
                        </div>
                      }
                      { item.featured_media === 0 &&
                        <div className="col s12 m12 l12">
                          <div className="item-post-date">
                            <p>{item.datep_field}</p>
                          </div>
                          <div className="itemm-post-title">
                            <h3>{item.title.rendered}</h3>
                          </div>
                          <div className="item-post-cont">
                            <div>{renderHTML(item.excerpt.rendered)}</div>
                          </div>
                        </div>
                      }  
                          
                      </div>
                    </div>
                  )
                })
              }
              </div>
              <div className="col s12 m5 l4 sidebar">
                <div className="sidebar__inner">
                  <div className="tagtwo">
                    <div className="container">
                      <div id="ad-slot-2"></div>
                    </div>
                  </div>

                  <ColumsContent colObj={this.state.columnObj}/>
                  <div className="tagthree">
                    <div className="container">
                      <div id="ad-slot-3"></div>
                    </div>
                  </div>
                  
                  <LectorContent lector={this.state.article}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className="page-footer">
          <div className="container">
            <div className="row">
              <div className="col s12 m3 l3 cont-logo">
                <a id="logo-container" href="/" className="brand-logo">
                  <img src="/wp-content/themes/beisbolplay/images/logo.png" alt=""/>
                </a>
              </div>
              <div className="col s12 m9 l9 right-align social-ico">
                <RedSocial />
              </div>
            </div>
            <div className="row">
              <div className="col s12 m4 l4 cont-logo-ter">
                <ul>
                  <li className="col s12 m7 l7"><a href="/terms-and-conditions/">TERMINOS DE USO</a></li>
                  <li className="col s12 m5 l5"><a href="mailto:contacto@beisbolplay.com">CONTACTO</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}


function mapStateToProps (reducer){
  return {
    pageTotal: reducer.page,
    post: reducer.post,
    menus: reducer.menus
  }
}

function mapDispathToProps (dispatch){
  return {
    getAllCategoryInit: (data) => dispatch(initCategories(data)),
    getAllPostInit: (data) => dispatch(initPostAsync(data)),
    getPagePost: (data, exclude) => dispatch(PostPageAsync(data, exclude)),
    getMenuInit: (data) => dispatch(initMenuAsync(data)),
    getCategory: (data) => dispatch(PostCategoryAsync(data)),
    filterBy: (data) => dispatch(filter(data)),
    orderBy: (data) => dispatch(order(data)),
  }
}

export default connect(mapStateToProps, mapDispathToProps)(App)
