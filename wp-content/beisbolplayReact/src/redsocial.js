import React, { Component } from 'react';
//import logo from './logo.svg';

class RedSocial extends Component {
  render() {
    return (
      <div>
        <a href="https://www.facebook.com/BeisbolPlayOnline/"><i className="fab fa-facebook-f" /></a>
        <a href="https://twitter.com/beisbolplayve?lang=en"><i className="fab fa-twitter" /></a>
        <a href="https://www.instagram.com/beisbolplayonline/"><i className="fab fa-instagram" /></a>
        <a href="https://www.youtube.com/beisbolplay"><i className="fab fa-youtube" /></a>
      </div>
    );
  }
}

export default RedSocial;
