import React, { Component } from 'react';
import RedSocial from './redsocial'
import Menu from './Menu'
import ColumsContent from './Columns'
import LectorContent from './Lector'
import ItemContent from './Itempost'


//import logo from './logo.svg';

import { connect } from 'react-redux'
import { initCategories, initPostAsync, initMenuAsync, order, filter, PostSlugAsync, PostnotSlugAsync, PostCategoryAsync } from './actions'

class Detailpost extends Component {
  constructor(props){
    super(props)
    this.state = {
      postObj:[],
      relObj:[],
      menu:[],
      article:[],
      loading: true,
      item:1,
      imgback: '',
      name: '',
      catName:'',
      relObjfill:[],
      objRel: [],
      columnObj:[],
      idCat:'',
      url:'',
    }
    this.handlescroll = this.handlescroll.bind(this)
    this.sidebarscroll = this.sidebarscroll.bind(this)
  }

  componentDidMount(){
    this.props.getMenuInit()
    this.props.getCategory(9)
    this.props.getCategory(12)
    window.addEventListener('scroll', this.handlescroll)
    window.addEventListener('scroll', this.sidebarscroll)
    this.setState({ 
      loading: false,
      url: `/${this.props.match.params.yearPath}/${this.props.match.params.monthPath}/${this.props.match.params.dayPath}/`
    })

    //this.props.getPostSlug(this.props.match.params.postPath)
    this.props.getPostNotSlug(this.props.match.params.postPath)
  }

  componentWillReceiveProps(next_props) {
    if(next_props.postObj !== this.state.postObj){
      next_props.category.map((item, i) =>{
        this.setState({
          idCat: item.id,
          catName: item.name
        })
        return true;
      })

      let menudata = next_props.postObj.filter((itemdata) => {
        for (var i = itemdata.categories.length - 1; i >= 0; i--) {
          return itemdata.categories[i] === 12
        }
        return true;
      })

      this.setState({
        article: menudata
      });
      let columnPost = next_props.postObj.filter((itemColumn) => {
        for (var i = itemColumn.categories.length - 1; i >= 0; i--) {
          return itemColumn.categories[i] === 9
        }
        return true;
      })

      let dat1=this.state.columnObj.length
      let dat2=columnPost.length

      if(dat1 < dat2){
        this.setState({
          columnObj: columnPost
        });
      }

      let post = next_props.postObj.filter((itemdata) => {
        return itemdata.slug === this.props.match.params.postPath
      })

      post.map((item, i) => {
        item.categories.map((cat, c) => {
          next_props.postObj.filter((itemdata) => {
            for (var i = itemdata.categories.length - 1; i >= 0; i--) {
              return itemdata.categories[i] === cat
            }
            return true
          })
          return true
        })
        return true
      })

      this.setState({
        postObj: post,
      });

      this.state.postObj.map((item, i) => {
        return this.setState({
          relObj: item.tags,
          nameact: item.title.rendered,
        })
      })

      this.setState({
        relObjfill: this.props.postRel
      })
    }
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handlescroll)
    window.removeEventListener('scroll', this.sidebarscroll)
  }

  handlescroll(event){
    if(this.state.loading) return null

    const scrolled = window.scrollY
    const viewportHeight = window.innerHeight;
    const fullHeight = document.body.clientHeight;

    if (!(scrolled + viewportHeight + 100 >= fullHeight)){
      return null
    }

    if(this.state.item < this.state.relObjfill.length && this.state.relObjfill.length > 0){
      let url = this.state.relObjfill[this.state.item].link.replace('http://aws.cloudpy.co', '')
      this.props.history.push(`${url}`)
      this.setState({ loading: true}, async () => {
        this.setState({
          objRel: this.state.objRel.concat(this.state.relObjfill[this.state.item]),
          loading:false,
          item: this.state.item +1
        })
      })
    }
  }

  sidebarscroll(event){
    if(this.state.loading) return null

    const scrolled = window.scrollY
    const viewportHeight = window.innerHeight
    const fullHeight = document.body.clientHeight
    const fullWidth = document.body.clientWidth
    const sidebarHeight = document.body.getElementsByClassName('sidebar__inner')[0].clientHeight
    const footerHeight = document.body.getElementsByClassName('page-footer')[0].clientHeight
    const sidebarPost = document.body.getElementsByClassName('sidebar')[0].offsetTop
    const totalPost = sidebarPost + sidebarHeight
    const viewHeigth = scrolled + viewportHeight
    const viewWindow = (fullHeight - footerHeight) - 50

    if(fullWidth > 430){

      if (!(scrolled + viewportHeight >= totalPost)){
        return null
      }

      if(viewHeigth >= totalPost && viewHeigth < viewWindow){
        let topNumber = viewHeigth - totalPost;
        document.body.getElementsByClassName('sidebar__inner')[0].style.top = `${topNumber}px`
      }
    }
  }

  render() {
    return (
      <div className="detail">
        <div className="section no-pad-bot" id="banner-up-tag">
          <div className="container">
            <div id="ad-slot"></div>
          </div>
        </div>
        <div className="cont-nav">
          <div className="lighten-1 hide-on-med-only">
            <div className="nav-wrapper container">
              <div className="row">
                <div className="col s6 offset-s6 right-align social-ico">
                  <RedSocial />
                </div>
              </div>
            </div>
          </div>
          <nav className="lighten-nav">
            <div className="nav-wrapper container">
              <Menu slug="menu"/>
            </div>
          </nav>
          <div className="cont-ban-detail">
          </div>
        </div>  
        <div className="section cont-post-description">
          <div className="container">
            <div className="row">
              <div className="col s12 m7 l8">
                <div className="cont-miga">
                  {
                    (this.state.catName.length > 0) &&
                    <p>{this.state.catName}</p>
                  }
                    
                    <p>{this.state.nameact}</p>
                  </div>  
                <ItemContent itemPost={this.state.postObj} urlact={this.state.url}/>
                { this.state.objRel.length > 0 && 
                  <ItemContent itemPost={this.state.objRel} urlact={this.state.url}/>
                }
              </div>
              <div className="col s12 m5 l4 sidebar">
                <div className="sidebar__inner">
                  <div className="tagtwo">
                    <div className="container">
                      <div id="ad-slot-2"></div>
                    </div>
                  </div>
                  <ColumsContent colObj={this.state.columnObj}/>
                  <div className="tagthree">
                    <div className="container">
                      <div id="ad-slot-3"></div>
                    </div>
                  </div>
                  <LectorContent lector={this.state.article}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className="page-footer">
          <div className="container">
            <div className="row">
              <div className="col s12 m3 l3 cont-logo">
                <a id="logo-container" href="/" className="brand-logo">
                  <img src="/wp-content/themes/beisbolplay/images/logo.png" alt=""/>
                </a>
              </div>
              <div className="col s12 m9 l9 right-align social-ico">
                <RedSocial />
              </div>
            </div>
            <div className="row">
              <div className="col s12 m4 l4 cont-logo-ter">
                <ul>
                  <li className="col s12 m7 l7"><a href="/terms-and-conditions/">TERMINOS DE USO</a></li>
                  <li className="col s12 m5 l5"><a href="mailto:contacto@beisbolplay.com">CONTACTO</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}


function mapStateToProps (reducer){
  return {
    pageTotal: reducer.page,
    postObj: reducer.post,
    menus: reducer.menus,
    category: reducer.categories,
    postRel: reducer.postrel,
  }
}

function mapDispathToProps (dispatch){
  return {
    getAllCategoryInit: (data) => dispatch(initCategories(data)),
    getAllPostInit: (data) => dispatch(initPostAsync(data)),
    getPostSlug: (data) => dispatch(PostSlugAsync(data)),
    getPostNotSlug: (data) => dispatch(PostnotSlugAsync(data)),
    getMenuInit: (data) => dispatch(initMenuAsync(data)),
    getCategory: (data) => dispatch(PostCategoryAsync(data)),
    filterBy: (data) => dispatch(filter(data)),
    orderBy: (data) => dispatch(order(data)),
  }
}

export default connect(mapStateToProps, mapDispathToProps)(Detailpost)
