import React, { Component } from 'react';
import RedSocial from './redsocial'
import Menu from './Menu'

import { connect } from 'react-redux'
import { initMenuAsync } from './actions'

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      menu:[],
    }
  }

  componentDidMount(){
    this.props.getMenuInit()

  }


  render() {
    return (
      <div className="detail category">
        <div className="section no-pad-bot" id="banner-up-tag">
          <div className="container">
            <div id="ad-slot"></div>
          </div>
        </div>
        <div className="cont-nav">
          <div className="lighten-1 hide-on-med-only">
            <div className="nav-wrapper container">
              <div className="row">
                <div className="col s6 offset-s6 right-align social-ico">
                  <RedSocial />
                </div>
              </div>
            </div>
          </div>
          <nav className="lighten-nav"  >
            <div className="nav-wrapper container">
              <Menu slug="menu"/>
            </div>
          </nav>
          <div className="cont-ban-detail">
          </div>
        </div>
        <div className="section cont-post">
          <div className="container">
            <div className="row">
              <div className="col s12 m12 l12">
                <div className="container-fluid error-cont container-fixed-lg sm-p-l-0 sm-p-r-0">
                  <div className="error-container text-center">
                    <h1 className="error-number hide">404</h1>
                    <h2 className="semi-bold">
                      <img src="/wp-content/themes/beisbolplay/images/404_es.png"  alt="Error 404"/>
                    </h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer className="page-footer">
          <div className="container">
            <div className="row">
              <div className="col s12 m3 l3 cont-logo">
                <a id="logo-container" href="/" className="brand-logo">
                  <img src="/wp-content/themes/beisbolplay/images/logo.png" alt=""/>
                </a>
              </div>
              <div className="col s12 m9 l9 right-align social-ico">
                <RedSocial />
              </div>
            </div>
            <div className="row">
              <div className="col s12 m4 l4 cont-logo-ter">
                <ul>
                  <li className="col s12 m7 l7"><a href="/terms-and-conditions/">TERMINOS DE USO</a></li>
                  <li className="col s12 m5 l5"><a href="mailto:contacto@beisbolplay.com">CONTACTO</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}


function mapStateToProps (reducer){
  return {
    menus: reducer.menus,
  }
}

function mapDispathToProps (dispatch){
  return {
    getMenuInit: (data) => dispatch(initMenuAsync(data)),
  }
}

export default connect(mapStateToProps, mapDispathToProps)(App)
