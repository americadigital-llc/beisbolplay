import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import Home from './Home'
import Detailpost from './Detailpost'
import Categorypost from './Categorypost'
import Tagspost from './Tagspost'
import Pages from './Pages'
import Error from './Error'

class App extends Component {
  
  render() { 
    return (
      <div>
        <Route exact path='/' render={(props) => (
          <Home {...props}/>
        )}/>
        <Route exact path='/:yearPath/:monthPath/:dayPath/:postPath' render={(props) => (
          <Detailpost {...props}/>
        )}/>
        <Route exact path='/category/:categoryPath' render={(props) => (
          <Categorypost {...props}/>
        )}/>
        <Route exact path='/tag/:tagPath' render={(props) => (
          <Tagspost {...props}/>
        )}/>
        <Route exact path='/:pagePath/' render={(props) => (
          <Pages {...props}/>
        )}/>
        <Route exact path='/error/404' render={(props) => (
          <Error {...props}/>
        )}/>
      </div>
    );
  }
}

export default App
