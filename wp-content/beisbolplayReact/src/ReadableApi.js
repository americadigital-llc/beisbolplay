
const api = "http://aws.cloudpy.co/wp-json/wp/v2"
const menuapi = "http://aws.cloudpy.co/wp-json/menus/v1"


// Generate a unique token for storing your bookshelf data on the backend server.
let token = localStorage.token
if (!token)
  token = localStorage.token = Math.random().toString(36).substr(-8)

const headers = {
  'Accept': 'application/json',
  'Authorization': token
}

export const getCategoriesAll = async () =>{

  const response = await fetch(`${api}/categories`, { headers })
  const json = await response.json();
  return json
}


export const getPostAll = async (exlude) => {
  let json =[]
  const response = await fetch(`${api}/posts?categories_exclude=${exlude}&order=desc&_embed`, { headers })
  json['page'] = response.headers.get("X-WP-TotalPages")
  json['data'] = await response.json()
  return json
}

export const getPostbyCategory =  async (catId) => {
  let json =[]
  const response = await fetch(`${api}/posts?categories=${catId}&order=desc&_embed`, { headers })
  json['page'] = response.headers.get("X-WP-TotalPages")
  json['data'] = await response.json()
  return json
}

export const getPostPage = async (page, exlude) => {
  const response = await fetch(`${api}/posts?page=${page}&categories_exclude=${exlude}&order=desc&_embed `, { headers })
  const json = response.json()
  return json
  
}

export const getPostPageCategory = async (page, idcat) => {
  const response = await fetch(`${api}/posts?page=${page}&categories=${idcat}&order=desc&_embed `, { headers })
  const json = response.json()
  return json
  
}

export const getPostbyId = (postId) =>
  fetch(`${api}/posts/${postId}`, { headers })
    .then(res => res.json())
    .then(data => data.post)

export const getPostbySlug = async (slug) => {
  const response = await fetch(`${api}/posts?slug=${slug}&_embed`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getMenuAll = async () => {
  const response = await fetch(`${menuapi}/menus`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getMenubySlug = async (slug) => {
  const response = await fetch(`${menuapi}/menus/${slug}`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getMediabyid = async (idMedia) => {
  const response = await fetch(`${api}/media/${idMedia}`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getTagsbySlug = async (slug) => {
  const response = await fetch(`${api}/tags?slug=${slug}`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getTagbyid = async (idTag) => {
  const response = await fetch(`${api}/tag/${idTag}`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getPostbyTagid = async (idTag, idPost) => {
  const response = await fetch(`${api}/posts?tags=${idTag}&order=desc&_embed`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getCategorybyid = async (idCat) => {
  const response = await fetch(`${api}/categories/${idCat}?order=desc`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}

export const getCategorybySlug = async (slug) => {
  const response = await fetch(`${api}/categories?slug=${slug}`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}


export const getPostbyCategorybyid = async (idCat) => {
  const response = await fetch(`${api}/posts?categories=${idCat}&order=desc&_embed`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}


export const getPagesbySlug = async (slug) => {
  const response = await fetch(`${api}/pages?slug=${slug}&_embed`, { headers, method: 'GET' })
  const json = await response.json()
  return json;
}





