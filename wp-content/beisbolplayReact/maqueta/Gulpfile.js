/*global -$ */
'use strict';
const gulp = require('gulp')
const $ = require('gulp-load-plugins')()
const sourcemaps = require('gulp-sourcemaps');
const del = require('del')
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const sassGlobing = require('node-sass-globbing');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;


gulp.task('styles:dev', function () {
  return gulp
    .src('./src/assets/css/main.scss')
    .pipe(concat('style.css'))
    .pipe(sass({
      outputStyle: 'uncompressed', // 'uncompressed',
      importer: sassGlobing
    })
    .on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(gulp.dest('../public/css/'))
    .pipe(reload({stream: true}));
});

gulp.task('styles:dist', function () {
  return gulp
    .src('./src/assets/css/main.scss')
    .pipe(concat('style.css'))
    .pipe(sass({
      outputStyle: 'compressed', // 'uncompressed',
      importer: sassGlobing
    })
    .on('error', sass.logError))
    .pipe(gulp.dest('./public/assets/css/'))
    .pipe(gulp.dest('../public/css/'));
    
});

gulp.task('clean', require('del').bind(null, ['dist']));

gulp.task('sync:site', () => {
  var serverConf = {
    server: {
      baseDir: "./public",
      directory: true
    },
    open: true,
    injectChanges: true,
    serveStatic: ['.', './public'],
    serveStaticOptions: {
        extensions: ['html'] // pretty urls
    }
  };
  browserSync.init(serverConf);
});


gulp.task('watch', function () {
  gulp.watch('src/assets/css/*.scss', ['styles:dev']);
  gulp.watch('src/assets/css/components/*.scss', ['styles:dev']);
  gulp.watch('public/index.html', ['styles:dev']);
});

gulp.task('default', ['styles:dev', 'sync:site', 'watch']);
gulp.task('server', ['styles:dist']);
