from timecoverspider.items import MagazineCover
import datetime
import scrapy
 
class CoverSpider(scrapy.Spider):
	name = "pyimagesearch-web-spider"
	start_urls = ["https://www.beisbolplay.com"]

	def parse(self, response):
		# let's only gather Time U.S. magazine covers
		url = response.css("div.refineCol ul li").xpath("a[contains(., 'TIME U.S.')]")
		yield scrapy.Request(url.xpath("@href").extract_first(), self.parse_page)